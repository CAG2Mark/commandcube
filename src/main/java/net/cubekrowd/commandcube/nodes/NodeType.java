package net.cubekrowd.commandcube.nodes;

public enum NodeType {
    ROOT,
    LITERAL,
    PLACEHOLDER;

}

/*

    commandcube
    Copyright (C) 2017  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.commandcube;

import java.util.List;
import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class CustomCommand extends Command {

    private final CommandCubePlugin plugin;
    @Getter
    private final CustomCommandData data;

    public CustomCommand(CommandCubePlugin plugin, String key, String[] aliases, CustomCommandData data) {
        super(key.toLowerCase(), null, aliases);
        this.plugin = plugin;
        this.data = data;
    }

    public void execute(CommandSender sender, String[] args) {
        for (String partKey : data.getSubKeys()) {

            // if no args, check if this key == ""
            if(args.length == 0 && !partKey.equals("")) {
                continue;
            }
            // if args, check if this is the correct key or asterisk key
            if(args.length > 0 && !partKey.equalsIgnoreCase(args[0]) && !partKey.equalsIgnoreCase("*")) {
                continue;
            }

            if(data.isForwardEnabled(partKey)) {
                if(sender instanceof ProxiedPlayer) {
                    ((ProxiedPlayer) sender).chat("/" + data.getKey() + " " + String.join(" ", args));
                } else {

                    sender.sendMessage(plugin.t(ChatColor.RED + "This is a forward-enabled command! Only player-sent commands can be forwared to the target server."));
                }
                return;
            }

            // command permission
            String permission = data.getPermission(partKey);
            if (permission != null && !plugin.checkPermission(sender, permission, true)) {
                return;
            }

            // command servers
            List<String> servers = data.getServers(partKey);
            if (sender instanceof ProxiedPlayer && !servers.contains("*") && !servers.contains(((ProxiedPlayer)sender).getServer().getInfo().getName())) {
                sender.sendMessage(plugin.t(ChatColor.RED + "This command is not available on the server you are on!"));
                return;
            }

            TextComponent message = data.getMessage(partKey, sender, args);
            if(message != null) {
                List<String> targets = data.getTargets(partKey);

                // send to each target
                for (String target : targets) {
                    // check send to self
                    if (target.equals("_self")) {
                        sender.sendMessage(message);
                        continue;
                    }
                    // check if the server exists
                    if (!plugin.getProxy().getServers().keySet().contains(target)) {
                        continue;
                    }
                    // send to server
                    plugin.getProxy().getServerInfo(target).getPlayers()
                            .forEach(pp -> pp.sendMessage(message));
                }
            }

            List<CustomCommandData.ExecutableCommand> commands = data.getCommands(partKey, sender, args);
            commands.forEach(ec -> {
                if(ec.isForward()) {
                    if(sender instanceof ProxiedPlayer) {
                        ((ProxiedPlayer) sender).chat(ec.getCommand());
                    } else {
                        sender.sendMessage(plugin.t(ChatColor.RED + "This command wants to execute a forwarded command! Only player-sent commands can be forwared to the target server."));
                    }
                    return;
                }

                // execute command
                String cmd = ec.getCommand();
                if(cmd.startsWith("/")) {
                    cmd = cmd.substring(1, cmd.length());
                }
                if(ec.isConsole()) {
                    plugin.getProxy().getPluginManager().dispatchCommand(plugin.getProxy().getConsole(), cmd);
                } else {
                    plugin.getProxy().getPluginManager().dispatchCommand(sender, cmd);
                }
            });
            return; // only execute first match
        }
    }

}

/*

    commandcube
    Copyright (C) 2017  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.commandcube.placeholder;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import net.cubekrowd.commandcube.CommandCubePlugin;
import net.cubekrowd.commandcube.Placeholder;
import net.md_5.bungee.api.CommandSender;

public class PlaceholderTime extends Placeholder {

    private final CommandCubePlugin plugin;

    public PlaceholderTime(CommandCubePlugin plugin) {
        super(plugin, "time", "Utility placeholder used to get the current time.", Arrays.asList("nano", "second", "minute", "hour", "day_of_week", "day_of_month", "day_of_year", "month", "year"));
        this.plugin = plugin;
    }

    public String getValue(CommandSender sender, String[] args) {
        if(args.length == 0) {
            return null;
        }

        OffsetDateTime odt = Instant.now().atOffset(ZoneOffset.UTC);
        if(args[0].equalsIgnoreCase("nano")) {
            return odt.getNano() + "";
        } else if(args[0].equalsIgnoreCase("second")) {
            return odt.getSecond() + "";
        } else if(args[0].equalsIgnoreCase("minute")) {
            return odt.getMinute() + "";
        } else if(args[0].equalsIgnoreCase("hour")) {
            return odt.getHour() + "";
        } else if(args[0].equalsIgnoreCase("day_of_week")) {
            return odt.getDayOfWeek() + "";
        } else if(args[0].equalsIgnoreCase("day_of_month")) {
            return odt.getDayOfMonth() + "";
        } else if(args[0].equalsIgnoreCase("day_of_year")) {
            return odt.getDayOfYear() + "";
        } else if(args[0].equalsIgnoreCase("month")) {
            return odt.getMonth() + "";
        } else if(args[0].equalsIgnoreCase("year")) {
            return odt.getYear() + "";
        }

        return null;
    }

}

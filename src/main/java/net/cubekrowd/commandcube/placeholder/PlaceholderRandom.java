package net.cubekrowd.commandcube.placeholder;

import net.cubekrowd.commandcube.CommandCubePlugin;
import net.cubekrowd.commandcube.Placeholder;
import net.md_5.bungee.api.CommandSender;

import java.util.Arrays;
import java.util.Random;

public class PlaceholderRandom extends Placeholder {

    private final CommandCubePlugin plugin;

    private final Random random = new Random();
    public PlaceholderRandom(CommandCubePlugin plugin) {
        super(plugin, "random", "Math placeholder to get a random number in a range.", Arrays.asList("<number> <number>"));
        this.plugin = plugin;
    }

    public String getValue(CommandSender sender, String[] args) {
        if (args.length == 2) {
            int min;
            int max;
            try {
                min = Integer.parseInt(args[0]);
            } catch (Exception e) {
                return "NaN";
            }
            try {
                max = Integer.parseInt(args[1]);
            } catch (Exception e) {
                return "NaN";
            }

            if (min > max) {
                int temp = max;
                max = min;
                min = temp;
            }
            int result = random.nextInt(max - min + 1) + min;
            return result + "";
        }

        return null;
    }

}

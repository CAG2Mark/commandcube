/*

    commandcube
    Copyright (C) 2017  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.commandcube;

import java.util.*;
import java.util.stream.*;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import net.cubekrowd.commandcube.CommandCubePlugin;
import net.md_5.bungee.api.*;
import net.md_5.bungee.api.chat.*;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.*;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.chat.ComponentSerializer;
import net.md_5.bungee.config.Configuration;
import lombok.*;

public class CustomCommandData {

    @Getter
    private final CommandCubePlugin plugin;
    @Getter
    private final String key;
    private final Configuration data;

    public CustomCommandData(CommandCubePlugin plugin, String key, Configuration data) {
        this.plugin = plugin;
        this.key = key.toLowerCase();
        this.data = data;
    }

    public Collection<String> getSubKeys() {
        return data.getKeys().stream()
               .filter(k -> !k.equalsIgnoreCase("forward"))
               .filter(k -> !k.equalsIgnoreCase("message"))
               .filter(k -> !k.equalsIgnoreCase("command"))
               .filter(k -> !k.equalsIgnoreCase("servers"))
               .filter(k -> !k.equalsIgnoreCase("targets"))
               .filter(k -> !k.equalsIgnoreCase("permission")).collect(Collectors.toList());
    }

    public TextComponent getMessage(@NonNull String sub, CommandSender sender, String[] args) {
        Configuration part = data.getSection(sub);

        // get message field
        Object objectMessage = part.get("message", null);
        Object objectCommand = part.get("command", null);
        if (objectCommand == null) {
            objectCommand = data.getString("command", null);
        }
        if (objectMessage == null) {
            if(objectCommand == null) {
                plugin.getLogger().severe(ChatColor.RED + "Error in command '" + key + "', missing message field");
                throw new RuntimeException("message");
            } else {
                return null;
            }
        }

        TextComponent message = null;
        if (objectMessage instanceof String) {
            // only a simple string message, simple parse
            if(sender == null || args == null) {
                message = plugin.blank((String) objectMessage);
            } else {
                message = plugin.blank(plugin.parsePlaceholders(sender, (String) objectMessage, args));
            }
        } else if (objectMessage instanceof List) {
            // list of object messages, complex parse
            message = new TextComponent();
            List<Map<String, String>> listMessage = (List<Map<String, String>>) objectMessage;
            for (Map<String, String> mapMessage : listMessage) {
                // text field
                if (!mapMessage.containsKey("text")) {
                    plugin.getLogger().severe(ChatColor.RED + "Error in command '" + key + "', missing message text field");
                    continue;
                }
                TextComponent extra;
                if(sender == null || args == null) {
                    extra = plugin.blank(mapMessage.get("text"));
                } else {
                    extra = plugin.blank(plugin.parsePlaceholders(sender, mapMessage.get("text"), args));
                }
                // clickurl field
                if (mapMessage.containsKey("clickurl")) {
                    extra.setClickEvent(new ClickEvent( ClickEvent.Action.OPEN_URL, mapMessage.get("clickurl")));
                }

                // permission field
                if (mapMessage.containsKey("permission")) {
                    if(!sender.hasPermission(mapMessage.get("permission"))) {
                        continue;
                    }
                }
                message.addExtra(extra);
            }
        }
        return message;
    }

    public List<ExecutableCommand> getCommands(@NonNull String sub, CommandSender sender, String[] args) {
        Configuration part = data.getSection(sub);

        // get command field
        Object objectCommand = part.get("command", null);
        if (objectCommand == null) {
            objectCommand = data.getString("command", null);
        }

        List<ExecutableCommand> commands = new ArrayList<>();
        if (objectCommand instanceof String) {
            // only a simple string command, simple parse
            ExecutableCommand cmd = new ExecutableCommand();
            if(sender == null || args == null) {
                cmd.setCommand((String) objectCommand);
            } else {
                cmd.setCommand(plugin.parsePlaceholders(sender, (String) objectCommand, args));
            }
            commands.add(cmd);
        } else if (objectCommand instanceof List) {
            // list of object commands, complex parse
            List<Map<String, Object>> listCommand = (List<Map<String, Object>>) objectCommand;
            for (Map<String, Object> mapCommand : listCommand) {
                // text field
                if (!mapCommand.containsKey("text")) {
                    plugin.getLogger().severe(ChatColor.RED + "Error in command '" + key + "', missing command text field");
                    continue;
                }
                ExecutableCommand cmd = new ExecutableCommand();
                if(sender == null || args == null) {
                    cmd.setCommand((String) mapCommand.get("text"));
                } else {
                    cmd.setCommand(plugin.parsePlaceholders(sender, (String) mapCommand.get("text"), args));
                }

                // permission field
                if (mapCommand.containsKey("permission")) {
                    cmd.setPermission((String) mapCommand.get("permission"));
                    if(sender != null && !sender.hasPermission(cmd.getPermission())) {
                        continue;
                    }
                }

                // forward field
                if (mapCommand.containsKey("forward")) {
                    cmd.setForward((boolean) mapCommand.get("forward"));
                }

                // console field
                if (mapCommand.containsKey("console")) {
                    cmd.setConsole((boolean) mapCommand.get("console"));
                }

                commands.add(cmd);
            }
        }
        return commands;
    }

    public String getPermission(@NonNull String sub) {
        Configuration part = data.getSection(sub);

        String permission = part.getString("permission", null);
        if (permission == null) {
            permission = data.getString("permission", null);
        }
        return permission;
    }

    public boolean isForwardEnabled(@NonNull String sub) {
        Configuration part = data.getSection(sub);

        boolean forward;
        if(part.contains("forward")) {
            forward  = part.getBoolean("forward", false);
        } else {
            forward = data.getBoolean("forward", false);
        }
        return forward;
    }

    public List<String> getServers(@NonNull String sub) {
        Configuration part = data.getSection(sub);

        List<String> servers = Arrays.asList((part.getString("servers", null) + "").split(","));
        if (servers.get(0).equals("null")) {
            servers = Arrays.asList((data.getString("servers", "*").split(",")));
        }
        return servers;
    }

    public List<String> getTargets(@NonNull String sub) {
        Configuration part = data.getSection(sub);

        List<String> targets = Arrays.asList((part.getString("targets", null) + "").split(","));
        if(targets.get(0).equals("null")) {
            targets = Arrays.asList((data.getString("targets", "_self")).split(","));
        }
        return targets;
    }

    @Data
    public class ExecutableCommand {
        private String command = null;
        private String permission = null;
        private boolean console = false;
        private boolean forward = false;
    }

}

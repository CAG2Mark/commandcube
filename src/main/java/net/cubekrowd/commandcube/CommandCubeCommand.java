/*

    commandcube
    Copyright (C) 2017  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.commandcube;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;

public class CommandCubeCommand extends Command {

    private final CommandCubePlugin plugin;
    private final String prefix = ChatColor.DARK_GRAY + "[" + ChatColor.AQUA + "C" + ChatColor.DARK_AQUA + "C" + ChatColor.DARK_GRAY + "] ";

    public CommandCubeCommand(CommandCubePlugin plugin) {
        super("commandcube", null, new String[0]);
        this.plugin = plugin;
    }

    public void execute(CommandSender sender, String[] args) {

        List<String> subsHelp = Arrays.asList("help", "reload", "commands", "command <command>", "placeholders", "placeholder <placeholder>", "credits");
        Consumer<CommandSender> printVersion = s -> s.sendMessage(plugin.t(prefix + ChatColor.GREEN + "Running version " + ChatColor.AQUA + plugin.getDescription().getVersion()));
        Consumer<CommandSender> printHelp = s -> subsHelp.forEach(sub -> s.sendMessage(plugin.t(prefix + ChatColor.DARK_GRAY + "/commandcube " + ChatColor.YELLOW + sub)));

        // print help
        if (args.length == 0 || (args.length != 0 && args[0].equalsIgnoreCase("help"))) {
            printVersion.accept(sender);
            if (sender.hasPermission("commandcube.admin")) {
                printHelp.accept(sender);
            }
            return;
        }

        // reload sub-command
        if (args[0].equalsIgnoreCase("reload")) {
            if (!plugin.checkPermission(sender,"commandcube.admin", true)) {
                return;
            }
            if (plugin.reloadFromConfig()) {
                sender.sendMessage(TextComponent.fromLegacyText(
                        prefix + ChatColor.GREEN + "Configuration, placeholders and commands reloaded."));
            } else {
                sender.sendMessage(TextComponent.fromLegacyText(
                        prefix + ChatColor.RED + "Failed to reload. See the console for details."));
            }
            return;
        }

        // commands sub-command
        if (args[0].equalsIgnoreCase("commands")) {
            if (!plugin.checkPermission(sender,"commandcube.admin", true)) {
                return;
            }

            var b = new ComponentBuilder("");
            var customCommands = plugin.customCommands;
            b.appendLegacy(prefix);
            b.append("Registered commands (" + customCommands.size() + "):",
                    ComponentBuilder.FormatRetention.NONE).color(ChatColor.GREEN);

            for (var c : customCommands) {
                var cd = c.getData();
                b.append("\n", ComponentBuilder.FormatRetention.NONE);
                b.appendLegacy(prefix);
                b.append("Command: ", ComponentBuilder.FormatRetention.NONE).color(ChatColor.DARK_GRAY);
                b.append(cd.getKey()).color(ChatColor.YELLOW);
                b.append(" Parts: ").color(ChatColor.DARK_GRAY);
                b.append(String.valueOf(cd.getSubKeys().size())).color(ChatColor.YELLOW);
            }

            sender.sendMessage(b.create());
            return;
        }

        // command sub-command
        if (args[0].equalsIgnoreCase("command") && args.length == 2) {
            if (!plugin.checkPermission(sender,"commandcube.admin", true)) {
                return;
            }

            String arg = args[1].toLowerCase();
            var customCommands = plugin.customCommands;
            CustomCommand match = null;

            customCommandsLoop:
            for (var c : customCommands) {
                if (c.getName().equalsIgnoreCase(arg)) {
                    match = c;
                    break;
                }

                for (var alias : c.getAliases()) {
                    if (alias.equalsIgnoreCase(arg)) {
                        match = c;
                        break customCommandsLoop;
                    }
                }
            }

            if (match == null) {
                sender.sendMessage(TextComponent.fromLegacyText(prefix
                        + ChatColor.RED + "That command doesn't exist!"));
                printVersion.accept(sender);
                printHelp.accept(sender);
                return;
            }

            // parse
            var ccd = match.getData();

            sender.sendMessage(plugin.t(prefix + ChatColor.GREEN + "Command information: " + ChatColor.AQUA + arg));
            for(String subKey : ccd.getSubKeys()) {
                sender.sendMessage(plugin.t(prefix + ChatColor.YELLOW + "  \"" + subKey + "\"" + ChatColor.DARK_GRAY +  ":"));
                sender.sendMessage(plugin.t(prefix + ChatColor.DARK_GRAY + "    Forward: " + ChatColor.YELLOW + ccd.isForwardEnabled(subKey)));
                sender.sendMessage(plugin.t(prefix + ChatColor.DARK_GRAY + "    Permission: " + ChatColor.YELLOW + ccd.getPermission(subKey)));
                sender.sendMessage(plugin.t(prefix + ChatColor.DARK_GRAY + "    Servers: " + ChatColor.YELLOW + String.join(", ", ccd.getServers(subKey))));
                sender.sendMessage(plugin.t(prefix + ChatColor.DARK_GRAY + "    Targets: " + ChatColor.YELLOW + String.join(", ", ccd.getTargets(subKey))));
                TextComponent tc = ccd.getMessage(subKey, null, null);
                if(tc == null) {
                    sender.sendMessage(plugin.t(prefix + ChatColor.DARK_GRAY + "    Message: " + ChatColor.YELLOW + "null"));
                } else {
                    String s = BaseComponent.toLegacyText(tc);
                    if(s.toString().contains("\n")) {
                        sender.sendMessage(new TextComponent(plugin.t(prefix + ChatColor.DARK_GRAY + "    Message: " + ChatColor.WHITE), new TextComponent(new ComponentBuilder("--hover-to-see-message--").event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[] {tc})).create())));
                    } else {
                        sender.sendMessage(new TextComponent(plugin.t(prefix + ChatColor.DARK_GRAY + "    Message: " + ChatColor.WHITE), tc));
                    }
                }

                List<CustomCommandData.ExecutableCommand> ecl = ccd.getCommands(subKey, null, null);
                sender.sendMessage(plugin.t(prefix + ChatColor.DARK_GRAY + "    Commands: " + (ecl.size() == 0 ? "[]" : "")));
                ecl.forEach(ec -> {
                    sender.sendMessage(prefix + ChatColor.GRAY + "    - Command: " + ChatColor.YELLOW + ec.getCommand());
                    sender.sendMessage(prefix + ChatColor.DARK_GRAY + "      Permission: " + ChatColor.YELLOW + ec.getPermission());
                    sender.sendMessage(prefix + ChatColor.DARK_GRAY + "      Console: " + (ec.isConsole() ? ChatColor.GREEN + "true" : ChatColor.RED + "false"));
                    sender.sendMessage(prefix + ChatColor.DARK_GRAY + "      Forward: " + (ec.isForward() ? ChatColor.GREEN + "true" : ChatColor.RED + "false"));
                    if(ec.isConsole() && ec.isForward()) {
                        sender.sendMessage(prefix + ChatColor.GOLD + "Notice: Console and foward are both set to true. A command which is forwarded to the Spigot-side cannot be forced-executed as console.");

                    }
                });
            }
            return;
        }

        // placeholders sub-command
        if (args[0].equalsIgnoreCase("placeholders")) {
            if (!plugin.checkPermission(sender,"commandcube.admin", true)) {
                return;
            }

            synchronized (plugin.globalLock) {
                sender.sendMessage(plugin.t(prefix + ChatColor.GREEN + "Registered placeholders (" + plugin.placeholders.size() + "):"));
                plugin.placeholders.forEach(p -> sender.sendMessage(plugin.t(prefix + ChatColor.DARK_GRAY + "Placeholder: " + ChatColor.YELLOW + p.getKey() + ChatColor.DARK_GRAY + " Plugin: " + ChatColor.YELLOW + p.getPlugin().getDescription().getName())));
            }
            return;
        }

        // placeholder sub-command
        if (args[0].equalsIgnoreCase("placeholder") && args.length == 2) {
            if (!plugin.checkPermission(sender,"commandcube.admin", true)) {
                return;
            }

            String arg = args[1].toLowerCase();
            Placeholder p;

            synchronized (plugin.globalLock) {
                p = plugin.placeholders.stream().filter(ph -> ph.getKey().equals(arg)).findFirst().orElse(null);
            }

            if(p == null) {
                sender.sendMessage(plugin.t(prefix + ChatColor.RED + "That placeholder doesn't exist!"));
                printVersion.accept(sender);
                printHelp.accept(sender);
                return;
            }

            sender.sendMessage(plugin.t(prefix + ChatColor.GREEN + "Placeholder information: " + ChatColor.AQUA + p.getKey()));
            sender.sendMessage(plugin.t(prefix + ChatColor.DARK_GRAY + "  Plugin: " + ChatColor.YELLOW + p.getPlugin().getDescription().getName()));
            sender.sendMessage(plugin.t(prefix + ChatColor.DARK_GRAY + "  Description: " + ChatColor.YELLOW + p.getDescription()));
            sender.sendMessage(plugin.t(prefix + ChatColor.DARK_GRAY + "  Usage: "));
            p.getUsage().forEach(u -> sender.sendMessage(prefix + ChatColor.GRAY + "  - " + ChatColor.YELLOW + "${" + p.getKey() + " " + u + "}"));
            return;
        }

        // commandcube sub-command
        if (args[0].equalsIgnoreCase("credits")) {
            sender.sendMessage(plugin.t(prefix + ChatColor.GREEN + "Credits:"));
            sender.sendMessage(plugin.t(prefix + ChatColor.DARK_GRAY + "Author: " + ChatColor.YELLOW + plugin.getDescription().getAuthor()));
            sender.sendMessage(plugin.t(prefix + ChatColor.DARK_GRAY + "Version: " + ChatColor.YELLOW + plugin.getDescription().getVersion()));
            sender.sendMessage(plugin.t(prefix + ChatColor.DARK_GRAY + "Source code: " + ChatColor.YELLOW + "https://gitlab.com/cubekrowd/plugins/" + plugin.getDescription().getName().toLowerCase()));
            sender.sendMessage(plugin.t(prefix + ChatColor.AQUA + "This is a free open-source plugin, originally developed for CubeKrowd. If you like it then please leave a review on the Spigot page. Thank you."));
            return;
        }

        // unknown command, print help
        sender.sendMessage(plugin.t(prefix + ChatColor.RED + "Invalid command!"));
        printVersion.accept(sender);
        printHelp.accept(sender);
    }
}

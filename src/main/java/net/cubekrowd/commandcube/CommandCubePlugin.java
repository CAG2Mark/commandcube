/*

    commandcube
    Copyright (C) 2017  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.commandcube;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.stream.Collectors;
import lombok.NonNull;
import net.cubekrowd.commandcube.nodes.NodeBuilder;
import net.cubekrowd.commandcube.nodes.RootNode;
import net.cubekrowd.commandcube.placeholder.CustomPlaceholder;
import net.cubekrowd.commandcube.placeholder.PlaceholderDivide;
import net.cubekrowd.commandcube.placeholder.PlaceholderGlobal;
import net.cubekrowd.commandcube.placeholder.PlaceholderIndex;
import net.cubekrowd.commandcube.placeholder.PlaceholderMinus;
import net.cubekrowd.commandcube.placeholder.PlaceholderModulo;
import net.cubekrowd.commandcube.placeholder.PlaceholderMultiply;
import net.cubekrowd.commandcube.placeholder.PlaceholderPlayer;
import net.cubekrowd.commandcube.placeholder.PlaceholderPlus;
import net.cubekrowd.commandcube.placeholder.PlaceholderRandom;
import net.cubekrowd.commandcube.placeholder.PlaceholderServer;
import net.cubekrowd.commandcube.placeholder.PlaceholderTime;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

public class CommandCubePlugin extends Plugin {
    public final Object globalLock = new Object();

    // always hold this globalLock when modifying accessing placeholders or
    // looping over placeholders
    public List<Placeholder> placeholders;

    // If you just need the read custom commands, assign this list to a local
    // variable and access that. There will be no race conditions and everything
    // will be thread safe if you do that.
    //
    // This list is never be null. Don't ever modify this list.
    public volatile List<CustomCommand> customCommands = Collections.emptyList();

    @Override
    public void onEnable() {
        saveDefaultConfig("config.yml");
        saveDefaultConfig("commands.yml");
        getProxy().getPluginManager().registerCommand(this, new CommandCubeCommand(this));
        reloadFromConfig();
    }

    @Override
    public void onDisable() {
        synchronized (globalLock) {
            placeholders = null;
            customCommands = Collections.emptyList();
        }
    }

    /**
     * Registers a place holder. You must hold the globalLock while running
     * this method to ensure changes happen in a thread-safe manner and no race
     * conditions occur.
     *
     * Registering a placeholder will fail if the key is not in lowercase or if
     * a placeholder with the same key has already been registered.
     *
     * @param placeholder the placeholder to register
     * @return {@code true} on success and {@code false} if something went wrong
     *         (see the console for details)
     */
    public boolean registerPlaceholderUnsafely(Placeholder placeholder) {
        if (placeholders == null) {
            throw new RuntimeException("Plugin is not enabled.");
        }

        var key = placeholder.getKey().toLowerCase(Locale.ENGLISH);
        if (!key.equals(placeholder.getKey())) {
            getLogger().warning("Illegal placeholder name '" + placeholder.getKey()
                    + "', placeholder names should be in all-lowercase.");
            return false;
        }

        for (var p : placeholders) {
            if (p.getKey().equals(key)) {
                getLogger().warning("Failed to register placeholder: '"
                        + placeholder.getKey() + "'. Already registered by: "
                        + p.getPlugin().getDescription().getName() + "."
                        + "Tried to be registered by: "
                        + placeholder.getPlugin().getDescription().getName());
                return false;
            }
        }

        placeholders.add(placeholder);
        placeholder.onRegister();
        return true;
    }

    public void saveDefaultConfig(String name) {
        getDataFolder().mkdirs();
        var configFile = new File(getDataFolder(), name);
        try {
            Files.copy(getResourceAsStream(name), configFile.toPath());
        } catch (FileAlreadyExistsException e) {
            // ignore
        } catch (IOException e) {
            getLogger().log(Level.WARNING, "Can't save default config " + name, e);
        }
    }

    public Configuration loadConfig(String name) {
        var configProvider = ConfigurationProvider.getProvider(YamlConfiguration.class);
        try {
            return configProvider.load(new File(getDataFolder(), name));
        } catch (IOException e) {
            getLogger().log(Level.WARNING, "Can't load config " + name, e);
            return new Configuration();
        }
    }

    public boolean reloadFromConfig() {
        synchronized (globalLock) {
            getLogger().info("Reloading configuration...");
            var configConfig = loadConfig("config.yml");
            var configCommands = loadConfig("commands.yml");
            boolean exit = false;

            if (configConfig.getInt("version") != 4) {
                getLogger().severe(ChatColor.RED + "WRONG CONFIGURATION VERSION. "
                        + "PLEASE BACKUP AND DELETE THE CONFIG.YML. The latest "
                        + "version will be generated automatically during next "
                        + "restart if you have moved the old one.");
                exit = true;
            }
            if (configCommands.getInt("version") != 3) {
                getLogger().severe(ChatColor.RED + "WRONG CONFIGURATION VERSION. "
                        + "PLEASE BACKUP AND DELETE THE COMMANDS.YML. The latest "
                        + "version will be generated automatically during next "
                        + "restart if you have moved the old one.");
                exit = true;
            }

            if (exit) {
                placeholders = null;
                customCommands = null;
                return false;
            }

            getLogger().info("Configuration reloaded.");

            getLogger().info("Removing old commands...");
            var oldCustomCommands = customCommands;
            customCommands = Collections.emptyList();

            for (var cc : oldCustomCommands) {
                getProxy().getPluginManager().unregisterCommand(cc);
            }

            getLogger().info("Loading placeholders...");
            // Copy old placeholders
            var oldPlaceholders = placeholders;
            placeholders = new ArrayList<>();

            var serverCountNotFound = configConfig.getString("server-count-not-found");
            var serverPlayerListNotFound = configConfig.getString("server-playerlist-not-found");
            var playerNameNotOnline = configConfig.getString("player-name-not-online");
            var playerServerNotOnline = configConfig.getString("player-server-not-online");

            // Add coded placeholders
            registerPlaceholderUnsafely(new PlaceholderServer(this,
                    serverCountNotFound, serverPlayerListNotFound));
            registerPlaceholderUnsafely(new PlaceholderGlobal(this));
            registerPlaceholderUnsafely(new PlaceholderPlayer(this,
                    playerNameNotOnline, playerServerNotOnline));
            registerPlaceholderUnsafely(new PlaceholderTime(this));
            registerPlaceholderUnsafely(new PlaceholderModulo(this));
            registerPlaceholderUnsafely(new PlaceholderPlus(this));
            registerPlaceholderUnsafely(new PlaceholderMinus(this));
            registerPlaceholderUnsafely(new PlaceholderMultiply(this));
            registerPlaceholderUnsafely(new PlaceholderDivide(this));
            registerPlaceholderUnsafely(new PlaceholderRandom(this));
            registerPlaceholderUnsafely(new PlaceholderIndex(this));

            // Add custom placeholders
            var placeholdersConfig = configConfig.getSection("placeholders");
            for (var placeholder : placeholdersConfig.getKeys()) {
                var placeholderConfig = placeholdersConfig.getSection(placeholder);
                if (!placeholderConfig.contains("description")) {
                    getLogger().warning("Illegal placeholder configuration '" + placeholder
                            + "', placeholder missing 'description' block.");
                    continue;
                }
                if (!placeholderConfig.contains("statements")) {
                    getLogger().warning("Illegal placeholder configuration '" + placeholder
                            + "', placeholder missing 'statements' block.");
                    continue;
                }
                if (!placeholderConfig.contains("usage")) {
                    getLogger().warning("Illegal placeholder configuration '" + placeholder
                            + "', placeholder missing 'usage' block.");
                    continue;
                }

                var statementsConfig = placeholderConfig.getSection("statements");
                var statements = statementsConfig.getKeys().stream()
                        .collect(Collectors.toMap(s -> s, statementsConfig::getString));
                var description = placeholderConfig.getString("description");
                var usage = placeholderConfig.getStringList("usage");

                var cp = new CustomPlaceholder(this, placeholder, description, usage, statements);
                registerPlaceholderUnsafely(cp);
            }

            // Re-add placeholders provided by other plugins
            if (oldPlaceholders != null) {
                oldPlaceholders.stream()
                        .filter(p -> !p.getPlugin().equals(this))
                        .forEach(this::registerPlaceholderUnsafely);
            }

            getLogger().info("Registering new commands...");
            Configuration commands = configCommands.getSection("commands");
            var keys = commands.getKeys();
            var newCustomCommands = new ArrayList<CustomCommand>(keys.size());

            for (var key : keys) {
                var cd = new CustomCommandData(this, key, commands.getSection(key));
                var aliases = key.toLowerCase(Locale.ENGLISH).split(",");
                var cc = new CustomCommand(this, aliases[0],
                        Arrays.copyOfRange(aliases, 1, aliases.length), cd);
                newCustomCommands.add(cc);
                getProxy().getPluginManager().registerCommand(this, cc);
            }

            customCommands = newCustomCommands;

            getLogger().info("Registered " + newCustomCommands.size() + " commands.");
            return true;
        }
    }

    public String parsePlaceholders(CommandSender sender, String input, String[] args) {
        NodeBuilder node = new NodeBuilder(input, this);
        RootNode root = node.build();
        return root.execute(sender, args);
    }

    public boolean checkPermission(CommandSender sender, String permission, boolean verbose) {
        if (sender.hasPermission(permission)) {
            return true;
        }
        if(verbose) {
            sender.sendMessage(t(ChatColor.RED + "You do not have permission to execute command!"));
        }
        return false;
    }

    public TextComponent t(@NonNull String s) {
        return new TextComponent(TextComponent.fromLegacyText( ChatColor.translateAlternateColorCodes('&', s)));
    }

    public TextComponent blank(@NonNull String s) {
        return new TextComponent(TextComponent.fromLegacyText(s));
    }
}

# CommandCube
![CommandCube](https://assets.gitlab-static.net/cubekrowd/commandcube/raw/master/docs/logotext.png "CommandCube")

CommandCube is BungeeCord plugin enabling you to dynamically specify custom commands without the need of restarting the server or coding a custom plugin. There is no limit on how many commands can be added, on what server they should work on or who should receive the message. Creating a new command is really simple, all you need to do is to edit the config with the custom values you want, reload and let the plugin do the work.  

### [Bugs & Feature Requests](https://gitlab.com/cubekrowd/commandcube/issues "Bugs & Feature Requests")

## Features

 * Per command permission support
 * Specify which servers the command should apply on
 * Specify targets, who should receive the message
 * Color support with custom specifiable messages
 * Command argument support
 * Reload configuration without restart
 * Network-wide broadcasts
 * Custom placeholders to dynamically customise messages

## Placeholders

The main structure of a placeholder is as following:  

`${placeholder_name arg1 arg2 arg3 ...}`

All placeholders start with a dollar-sign are followed by curly-brackets. The first word inside 
the brackets is the name of the placeholder. Anything else inside it are arguments passed to the 
placeholder at execution. The arguments is separated by a space, if you have an argument which 
contains a space then surround it with quotation marks "hi there". Placeholders can be used inside 
any output string, or even inside each other: (explanation of what each placeholder does below)

 * `${plus ${server count lobby1} ${server count server2}}` - Returns the number of people in lobby1 and lobby2

The plugin has support for dynamically created placeholders in the config.yml, as well as allowing
plugins to register their own placeholders. A few are already made by the plugin itself:
<> - required, [] - optional  

 * `${global count}` - Returns the number of people online globally.
 * `${global playerlist}` - Returns a comma-separated list of all players online.
 * `${player name [player]}` - Returns the correct capitalized name of a player, sender unless specified.
 * `${player server [player]}` - Returns the server a player is on, sender unless otherwise specified.
 * `${player ping}` - Returns the ping in ms of the player executing the command. Other players are not yet implemented.
 * `${server count [server]}` - Returns the server count of of a specified server, server of sender if none specified.
 * `${server playerlist [server]}` - Returns a comma-separated list of all players on a server, server of server if none specified.
 * `${time nano}` - Current nanoseconds of the second.
 * `${time second}` - Current seconds of the minute.
 * `${time minute}` - Current minutes of the hour.
 * `${time hour}` - Current hours of the day.
 * `${time day_of_week}` - Current day of the week.
 * `${time day_of_month}` - Current day of the month.
 * `${time day_of_year}` - Current day of the year.
 * `${time month}` - Current month of the year
 * `${time year}` - Current year.
 * `${plus <number1> <number2>}` - Adds two numbers together.
 * `${minus <number1> <number2>}` - Subtracts two numbers.
 * `${modulo <dividend> <divisor>}` - Takes the modulus of two numbers.

## Installation

 1. Drop the plugin into the plugin folder on BungeeCord and restart.
 2. Specify your custom commands in the config file.
 3. Do `/commandcube reload` for it to take affect.

## Commands

`/commandcube help` - Prints the help menu  
`/commandcube reload` - Reloads the commands, placeholders and the configuration  
`/commandcube commands` - List all custom commands  
`/commandcube command <command>` - Prints information about a specific custom command  
`/commandcube commands` - List all registered commands  
`/commandcube command <command>` - Prints information about a specific placeholder  
`/commandcube credits` - Prints plugin credits  

## Permissions

This plugin only has one predefined permission node by default, which is:  

`commandcube.admin` - Allows access to all information and management commands.

It is possible to for each custom command specify custom permission nodes. The custom 
permission nodes can be whatever you want and are therefore obviously not listed here.

## Configuration

### Commands

Commands are can be custom defined in the `commands.yml` file. Both the config file and the 
commands file uses YML and it is very important they are correctly formatted before you save.
If you do not know how to use YML then please read a simple Bukkit tutorial before asking for help. It will save everyones time.

Each 'block' starts with the name of the command. A command can have multiple aliases, if so 
then separate them with commas, remember to put the string inside quotes. Each sub-block is a 
sub command. Empty quotes "" means that block will be executed if no arguments are provided. 
An asterisk means it will get executed regardless of what the first argument is. There are also
placeholders for each argument, ${1} refers to the first argument, ${2} to the second, etc.. ${?}
refers to all arguments.

Targets refer to which servers should receive the messages. The default `_self` refers to the sender. 
Servers refer to which servers the command should work on. The default is `*` which means all. 
Forward decides if the command should be executed or forwarded. This might be handy if you want a 
custom command to execute `/help Custom` but want Spigot to execute the generic `/help`. Messages 
can either be single line strings or multiline message-objects. Please see the below example for usage:

```yml
  find:
    "":
      message: "&cYou must specify a player name."
    "*":
      message: "${find ${1}}"
      permission: "bungeecord.command.find"
  "help,helpme":
    link:
      message:
      - {text: "&b ----- &6Help Page -----"}
      - {text: "&aClick herer to view the &ehelp &apage.", clickurl: "https://example.com/help"}
  say:
    "":
      forward: true
    "*":
      servers: "lobby,creative,survival,skyblock"
      targets: "lobby,creative,survival,skyblock"
      permission: example.command.say
      message: "&d${player name} says: ${?}"
  abc:
    "":
      command:
      - {text: "/send ${player name}", console: true, permission: "customcommand.abc"}
      - {text: "/somecommand", forward: true}
```

Version 2.3 also added the posibility to make custom commands execute other custom commands. 
Commands can either follow the simple layout `commands: "/help"` or it can follow the complex 
layout shown in the ABC example command. Note that the `targets` setting is only for text messages 
and does not apply for the commands. If you want to make the command execute on the server a player 
is on then enable forward to true. Commands which are forwarded cannot be executed as console. 
The permission can be anything you want, however, it is recommended it starts with "customcommand" 
to prevent collisions with other plugins.

### Placeholders

Placeholders are like truth tables (or switch cases if you are a programmer). Each placeholder 
has a name, description, usage and statements. Each 'block' starts with the name of the placeholder.
The description and the usage is merely for informative purposes and does not affect the workings of 
the placeholder. Lastly the statements decide what the placeholder will return. On the left side there 
is a condition, every condition is required to have two equal signs `==`, if the content on the left 
matches the content on the right then that result is returned. If none of the statements are true then 
the `default` statement is returned. All placeholders are required to have a `default` line.

Examples:
```yml
placeholders:
  find:
    description: "Prints out a message stating what server a specified player is on."
    usage:
    - "<player>"
    statements:
      "${player name ${1}} == -": "&cNo such player found."
      default: "&9${player name ${1}} is on ${player server ${player name ${1}}}"
  random_joke:
    description: "Returns a random joke. Call the placeholder with ${random_joke ${time nano}}"
    usage:
    - "<number>"
    statements:
      "${modulus ${1} 3} == 0": "JOKE 1"
      "${modulus ${1} 3} == 1": "JOKE 2"
      "${modulus ${1} 3} == 2": "JOKE 3"
      default: "&cERROR"
```

## Credits & Support

This plugin is Open-Source, released under AGPL-3.0.
If you want to contribute you can find the repository at:  

[GitLab](https://gitlab.com/cubekrowd/commandcube "[GitLab]")
